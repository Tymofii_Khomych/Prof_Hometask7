﻿namespace task2
{
    // для завдання 2
    public class ShowToUserAttribute : Attribute
    {
        public ShowToUserAttribute()
        {
        }
    }

    public class MyClass1
    {
        [ShowToUser]
        public void Method1()
        {
            Console.WriteLine("Method 1");
        }

        [ShowToUser]
        public void Method2()
        {
            Console.WriteLine("Method 2");
        }

        public void Method3()
        {
            Console.WriteLine("Method 3");
        }

        [ShowToUser]
        public string Property1 { get; set; }

        public string Property2 { get; set; }

        [ShowToUser]
        public string Property3 { get; set; }
    }

    public class MyClass
    {
        [Obsolete("This method is obsolete. Please use NewMethod instead.")]
        public void OldMethod()
        {
            Console.WriteLine("This is the old method.");
        }

        [Obsolete("This method is obsolete and should not be used.", true)]
        public void DeprecatedMethod()
        {
            Console.WriteLine("This method is deprecated.");
        }

        public void NewMethod()
        {
            Console.WriteLine("This is the new method.");
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            MyClass obj = new MyClass();

            obj.OldMethod(); 
            // obj.DeprecatedMethod(); 
            obj.NewMethod(); 

            Console.ReadLine();
        }
    }
}
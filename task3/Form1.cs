using System.Reflection;
using task2;
namespace task3
{
    public partial class Form1 : Form
    {
        Assembly assembly = null;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                string path = openFileDialog1.FileName;

                try
                {
                    assembly = Assembly.LoadFile(path);
                    label1.Text += "������    " + path + "  -  ������� ���������" + Environment.NewLine + Environment.NewLine;
                }
                catch (FileNotFoundException ex)
                {
                    Console.WriteLine(ex.Message);
                }

                label1.Text += "������ ���� ����� � ������:     " + assembly.FullName + Environment.NewLine + Environment.NewLine;

                Type[] types = assembly.GetTypes();

                foreach (Type type in types)
                {
                    label1.Text += "���:  " + type + Environment.NewLine;

                    MethodInfo[] methods = type.GetMethods();
                    PropertyInfo[] properties = type.GetProperties();

                    // ���� ����������� ������ ����
                    var showMethods = methods.Where(m => m.GetCustomAttributes(typeof(ShowToUserAttribute), false).Length > 0);
                    var showProperties = properties.Where(p => p.GetCustomAttributes(typeof(ShowToUserAttribute), false).Length > 0);

                    foreach (var method in showMethods)
                    {
                        string methStr = "�����:" + method.Name + "\n";
                        var methodBody = method.GetMethodBody();
                        if (methodBody != null)
                        {
                            var byteArray = methodBody.GetILAsByteArray();

                            foreach (var b in byteArray)
                            {
                                methStr += b + ":";
                            }
                        }
                        label1.Text += methStr + Environment.NewLine;
                    }

                    foreach (var property in showProperties)
                    {
                        label1.Text += "����������: " + property.Name + Environment.NewLine;
                    }
                }
            }
        }
   
    }
}
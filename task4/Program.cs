﻿namespace task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // https://learn.microsoft.com/ru-ru/dotnet/csharp/advanced-topics/reflection-and-attributes/attribute-tutorial
            // Определение и чтение настраиваемых атрибутов
            /* 
             * Добавление атрибутов в код
             * В C# атрибуты представляют собой классы, наследующие от базового класса Attribute. Любой класс, который наследует от Attribute, 
             * можно использовать как своего рода "тег" на другие части кода. Например, существует атрибут с именем ObsoleteAttribute. Этот атрибут 
             * сигнализирует о том, что код устарел и больше не должен использоваться. Этот атрибут помещается в класс, например, с помощью квадратных 
             * скобок.
             * 
             * [Obsolete]
             * public class MyClass
             * {}
             * 
             * Хотя класс называется ObsoleteAttribute, его необходимо только использовать [Obsolete] в коде. Большинство кода C# соответствует этому соглашению. 
             * При желании вы можете использовать полное имя [ObsoleteAttribute].
             * Когда вы отмечаете класс как устаревший, желательно предоставить некоторые сведения о том, почему он устарел и (или) что можно использовать вместо него. 
             * Чтобы предоставить это объяснение, необходимо добавить строковый параметр в атрибут Obsolete.
             * 
             * [Obsolete("ThisClass is obsolete. Use ThisClass2 instead.")]
             * public class ThisClass
             * {}
             * 
             * Строка передается в качестве аргумента ObsoleteAttribute в конструктор, как если бы вы писали var attr = new ObsoleteAttribute("some string").
             * В конструкторе атрибута можно использовать в качестве параметров только простые типы и литералы bool, int, double, string, Type, enums, etc и массивы этих типов. 
             * Нельзя использовать выражение или переменную. Вы можете использовать позиционные или именованные параметры.
             * 
             * Создание собственного атрибута
             * Атрибут создается путем определения нового класса, наследуемого Attribute от базового класса.
             * public class MySpecialAttribute : Attribute{}
             * 
             * В приведенном выше коде можно использовать [MySpecial] (или [MySpecialAttribute]) в качестве атрибута в другом месте в базе кода.
             * [MySpecial]
             * public class SomeOtherClass
             * {
             * }
             * Атрибуты в библиотеке базовых классов .NET, например ObsoleteAttribute, активируют определенный действия компилятора. Но созданные вами 
             * атрибуты сами по себе лишь выполняют роль метаданных и не влекут за собой исполнение какого-либо кода в классе атрибута. Вы должны 
             * действовать на основе метаданных в другом месте кода.
             * 
             * Здесь есть "gotcha", чтобы watch для. Как упоминалось ранее, при использовании атрибутов в качестве аргументов могут передаваться только 
             * определенные типы. Однако при создании типа атрибута компилятор C# не мешает создавать эти параметры. В следующем примере вы создали атрибут 
             * с конструктором, который правильно компилируется.
             * 
             * public class GotchaAttribute : Attribute
             * {
             *       public GotchaAttribute(Foo myClass, string str)
             *       {
             *       }
             * }    


             */
        }
    }
}
﻿namespace task5
{
    internal class Program
    {
        public class AccessLevelAttribute : Attribute
        {
            public int Level { get; }

            public AccessLevelAttribute(int level)
            {
                Level = level;
            }
        }

        public abstract class Employee
        {
            public string Name { get; }

            public Employee(string name)
            {
                Name = name;
            }

            public virtual void AccessRestrictedSection()
            {
                Console.WriteLine($"{Name} has access to the restricted section.");
            }
        }

        [AccessLevel(1)]
        public class Manager : Employee
        {
            public Manager(string name) : base(name)
            {
            }
        }

        [AccessLevel(2)]
        public class Programmer : Employee
        {
            public Programmer(string name) : base(name)
            {
            }
        }

        [AccessLevel(3)]
        public class Director : Employee
        {
            public Director(string name) : base(name)
            {
            }
        }

        static void CheckAccess(Employee employee)
        {
            Type type = employee.GetType();
            AccessLevelAttribute attribute = (AccessLevelAttribute)Attribute.GetCustomAttribute(type, typeof(AccessLevelAttribute));

            if (attribute != null)
            {
                Console.WriteLine($"Checking access for {employee.Name} (Level {attribute.Level})");
                if (attribute.Level >= 2)
                {
                    employee.AccessRestrictedSection();
                }
                else
                {
                    Console.WriteLine($"{employee.Name} does not have access to the restricted section.");
                }
            }
            else
            {
                Console.WriteLine($"Access level attribute not found for {employee.Name}.");
            }

            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            Employee manager = new Manager("John");
            Employee programmer = new Programmer("Alice");
            Employee director = new Director("Tom");

            CheckAccess(manager);
            CheckAccess(programmer);
            CheckAccess(director);
        }
    }
}